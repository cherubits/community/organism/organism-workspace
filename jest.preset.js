const nxPreset = require('@nx/jest/preset').default;

module.exports = {
  ...nxPreset,
  moduleNameMapper: {
    '^d3$': '<rootDir>/../../../node_modules/d3/dist/d3.min.js',
  },
};
