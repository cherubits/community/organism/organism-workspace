#!/bin/bash

export DRY_RUN=0
export NX_COMMAND_SUFFIX=""
export PROJECT_SCOPE="organism"

if [ $DRY_RUN == "1" ]; then
  export NX_COMMAND_SUFFIX="--dry-run"
fi

function nx_generate_nest_library() {
  local SCOPE=$1
  local MODULE=$2
  local ARCHETYPE=$3
  yarn nx generate @nx/nest:library \
    --name=${MODULE}-${ARCHETYPE} \
    --buildable=true \
    --publishable=true \
    --directory=libs/${MODULE}/${ARCHETYPE} \
    --importPath=@${SCOPE}/${MODULE}-${ARCHETYPE} \
    --projectNameAndRootFormat=as-provided \
    --service=true \
    --tags="module:${MODULE},archetype:${ARCHETYPE}" \
    --no-interactive ${NX_COMMAND_SUFFIX}
}

function nx_generate_nest_application() {
  local SCOPE=$1
  local MODULE=$2
  local ARCHETYPE=$3
  local FE=$4
  yarn nx generate @nx/nest:application \
    --name=${MODULE}-${ARCHETYPE} \
    --frontendProject=${FE} \
    --directory=apps/${MODULE}/${ARCHETYPE} \
    --projectNameAndRootFormat=as-provided \
    --strict=true \
    --tags="module:${MODULE},archetype:${ARCHETYPE}" \
    --no-interactive ${NX_COMMAND_SUFFIX}
}

function nx_generate_react_frontend() {
  local SCOPE=$1
  local MODULE=$2
  local ARCHETYPE=$3
  yarn nx generate @nx/react:application \
    --name=${MODULE}-${ARCHETYPE} \
    --directory=apps/${MODULE}/${ARCHETYPE} \
    --pascalCaseFiles=true \
    --projectNameAndRootFormat=as-provided \
    --routing=true \
    --style=@emotion/styled \
    --tags="module:${MODULE},archetype:${ARCHETYPE}" \
    --no-interactive ${NX_COMMAND_SUFFIX}
}

function nx_generate_react_library() {
  local SCOPE=$1
  local MODULE=$2
  local ARCHETYPE=$3
  local FE=$4
  yarn nx generate @nx/react:library \
    --name=${MODULE}-${ARCHETYPE} \
    --bundler=rollup \
    --directory=libs/${MODULE}/${ARCHETYPE} \
    --appProject=${FE} \
    --importPath=@${SCOPE}/${MODULE}-${ARCHETYPE} \
    --pascalCaseFiles=true \
    --projectNameAndRootFormat=as-provided \
    --publishable=true \
    --style=@emotion/styled \
    --tags="module:${MODULE},archetype:${ARCHETYPE}" \
    --no-interactive ${NX_COMMAND_SUFFIX}
}

function nx_generate_storybook() {
  local SCOPE=$1
  local MODULE=$2
  local ARCHETYPE=$3
  yarn nx generate @nx/storybook:configuration \
    --name=${MODULE}-${ARCHETYPE} \
    --uiFramework=@storybook/react-webpack5 \
    --configureStaticServe=true \
    --no-interactive ${NX_COMMAND_SUFFIX}
}

function nx_generate_library() {
  local SCOPE=$1
  local MODULE=$2
  local ARCHETYPE=$3
  yarn nx generate @nx/js:library \
    --name=${MODULE}-${ARCHETYPE} \
    --unitTestRunner=jest \
    --directory=libs/${MODULE}/${ARCHETYPE} \
    --importPath=@${SCOPE}/${MODULE}-${ARCHETYPE} \
    --publishable=true \
    --includeBabelRc=true \
    --projectNameAndRootFormat=as-provided \
    --testEnvironment=jsdom  \
    --tags="module:${MODULE},archetype:${ARCHETYPE}" \
    --no-interactive ${NX_COMMAND_SUFFIX}
}

function nx_ddd() {
  local MODULE=$1
  nx_generate_react_frontend ${PROJECT_SCOPE} ${MODULE} "frontend"
  nx_generate_react_library ${PROJECT_SCOPE} ${MODULE} "pres" "${MODULE}-frontend"
  nx_generate_storybook ${PROJECT_SCOPE} ${MODULE} "pres"
  nx_generate_nest_library ${PROJECT_SCOPE} ${MODULE} "domain"
  nx_generate_nest_library ${PROJECT_SCOPE} ${MODULE} "infra"
  nx_generate_nest_library ${PROJECT_SCOPE} ${MODULE} "app"
  nx_generate_nest_application ${PROJECT_SCOPE} ${MODULE} "backend" "${MODULE}-frontend"
  nx_generate_library ${PROJECT_SCOPE} ${MODULE} "common"
  nx_generate_library ${PROJECT_SCOPE} ${MODULE} "client"
}



function nx_workspace() {
  # nx_ddd "uaac"
  # nx_ddd "instructor"
  # nx_ddd "schef"
  # nx_ddd "treasurer"
  # nx_ddd "alchemyst"
  # nx_ddd "worksheet"
  # nx_ddd "core"
  nx_ddd "cms"
}

nx_workspace
