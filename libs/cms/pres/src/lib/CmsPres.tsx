import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface CmsPresProps {}

const StyledCmsPres = styled.div`
  color: pink;
`;

export function CmsPres(props: CmsPresProps) {
  return (
    <StyledCmsPres>
      <h1>Welcome to CmsPres!</h1>
    </StyledCmsPres>
  );
}

export default CmsPres;
