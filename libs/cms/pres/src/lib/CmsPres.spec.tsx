import { render } from '@testing-library/react';

import CmsPres from './CmsPres';

describe('CmsPres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CmsPres />);
    expect(baseElement).toBeTruthy();
  });
});
