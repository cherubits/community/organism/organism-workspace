```mermaid
erDiagram

  "currencies" {
    String code "🗝️"
    String friendly_name "❓"
    }
  

  "locales" {
    String id "🗝️"
    String currency_code 
    String country 
    String language 
    Boolean enabled 
    }
  
    "currencies" o{--}o "locales" : "Locale"
    "locales" o|--|| "currencies" : "currency"
```
