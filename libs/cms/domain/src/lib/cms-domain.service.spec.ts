import { Test } from '@nestjs/testing';
import { CmsDomainService } from './cms-domain.service';

describe('CmsDomainService', () => {
  let service: CmsDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CmsDomainService],
    }).compile();

    service = module.get(CmsDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
