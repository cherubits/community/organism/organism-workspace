import { Module } from '@nestjs/common';
import { CmsDomainService } from './cms-domain.service';

@Module({
  controllers: [],
  providers: [CmsDomainService],
  exports: [CmsDomainService],
})
export class CmsDomainModule {}
