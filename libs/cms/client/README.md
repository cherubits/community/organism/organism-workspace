# cms-client

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build cms-client` to build the library.

## Running unit tests

Run `nx test cms-client` to execute the unit tests via [Jest](https://jestjs.io).
