# cms-infra

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build cms-infra` to build the library.

## Running unit tests

Run `nx test cms-infra` to execute the unit tests via [Jest](https://jestjs.io).
