import { Module } from '@nestjs/common';
import { CmsInfraService } from './cms-infra.service';

@Module({
  controllers: [],
  providers: [CmsInfraService],
  exports: [CmsInfraService],
})
export class CmsInfraModule {}
