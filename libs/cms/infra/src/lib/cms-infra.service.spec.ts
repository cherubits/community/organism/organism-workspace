import { Test } from '@nestjs/testing';
import { CmsInfraService } from './cms-infra.service';

describe('CmsInfraService', () => {
  let service: CmsInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CmsInfraService],
    }).compile();

    service = module.get(CmsInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
