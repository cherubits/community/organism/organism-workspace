import { Test } from '@nestjs/testing';
import { CmsAppService } from './cms-app.service';

describe('CmsAppService', () => {
  let service: CmsAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CmsAppService],
    }).compile();

    service = module.get(CmsAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
