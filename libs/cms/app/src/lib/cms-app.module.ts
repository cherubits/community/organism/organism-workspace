import { Module } from '@nestjs/common';
import { CmsAppService } from './cms-app.service';

@Module({
  controllers: [],
  providers: [CmsAppService],
  exports: [CmsAppService],
})
export class CmsAppModule {}
