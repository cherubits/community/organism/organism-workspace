import { coreClient } from './core-client';

describe('coreClient', () => {
  it('should work', () => {
    expect(coreClient()).toEqual('core-client');
  });
});
