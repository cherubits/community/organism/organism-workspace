// Replace your-framework with the framework you are using (e.g., react, vue3)
import { ThemeProvider, createTheme } from '@mui/material';
import { Preview } from '@storybook/react';
import { themes } from '@storybook/theming';
import '../src/styles/tailwind.css'

const theme = createTheme();

const preview: Preview = {
  parameters: {
    layout: 'centered',
    docs: {
      theme: themes.dark,
    },
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
  decorators: [
    (Story) => (
      <ThemeProvider theme={theme}>
        <Story />
      </ThemeProvider>
    ),
  ],
};

export default preview;
