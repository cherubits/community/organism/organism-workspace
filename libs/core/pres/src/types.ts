export type SearchTermType = string | ReadonlyArray<string> | number;
