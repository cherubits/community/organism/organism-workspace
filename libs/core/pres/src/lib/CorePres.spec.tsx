import { render } from '@testing-library/react';

import CorePres from './CorePres';

describe('CorePres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CorePres />);
    expect(baseElement).toBeTruthy();
  });
});
