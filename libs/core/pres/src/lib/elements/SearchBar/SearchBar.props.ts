import { ChangeEvent } from "react";

export interface SearchBarProps {
  value?: string | ReadonlyArray<string> | number;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  onSubmit?: () => void;
  className?: string;
  inputClassName?: string;
  placeholder?: string;
}
