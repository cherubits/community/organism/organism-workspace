import { ChangeEvent, FC, useEffect, useState } from 'react';
import clsx from 'clsx';
import { SearchBarProps } from './SearchBar.props';
import { Search } from '@emotion-icons/feather';
import { SearchTermType } from '../../../types';



import styled from '@emotion/styled'

export const SearchBarIcon = styled(Search)`
  /* color: red;

  font-weight: ${(props: any) => (props.important ? 'bold' : 'normal')}; */
`

export const SearchBar: FC<SearchBarProps> = ({
  className = 'h-[44px] w-96 bg-white',
  inputClassName = 'text-sm text-black px-2',
  onChange,
  onSubmit,
  placeholder = 'Search',
  value,
}: SearchBarProps) => {
  return (
    <form className={clsx('flex items-center rounded px-4 cursor-pointer', className)} onSubmit={(e) => onSubmit?.()}>
      {/* <Search className='w-16px' /> */}
      <SearchBarIcon size={24} />
      <input
        type="search"
        placeholder={placeholder}
        onChange={(e) => onChange?.(e)}
        onKeyDown={(e) => {
          if (e.key === 'Enter') {
            onSubmit?.();
          }
        }}
        value={value}
        className={clsx(
          'w-full border-none outline-none focus:ring-0',
          inputClassName
        )}
      />
    </form>
  );
};

export default SearchBar;
