import type { Meta, StoryObj } from '@storybook/react';
import { SearchBar } from './SearchBar';

import { userEvent, within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof SearchBar> = {
  component: SearchBar,
  title: 'Elements/SearchBar',
};
export default meta;
type Story = StoryObj<typeof SearchBar>;

export const Primary: Story = {
  args: {},
};

export const Secondary: Story = {
  args: {},
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    // expect(canvas.getByText(/Welcome to BasicAppLayout!/gi)).toBeTruthy();

    const searchInput = canvas.getByPlaceholderText('Search')
    searchInput.focus();
    await userEvent.keyboard('haha\n')
  },
};
