import clsx from 'clsx';
import { FC } from 'react';
import { BreadcrumbsProps } from './Breadcrumbs.props';
import { Link } from '@mui/material';


export const Breadcrumbs: FC<BreadcrumbsProps> = ({
  items,
  className,
  children
}: BreadcrumbsProps) => {
  return (
    <div className={clsx('text-sm flex items-center gap-x-2', className)}>
      {items.map((item, index) => (
        <div key={index}>
          {item.link ? (
            <Link href={item.link}>{item.title}</Link>
          ) : (
            <span>{item.title}</span>
          )}
          {index !== items.length - 1 && (
             <span className='pl-2'>
              {children || (<span>|</span>)}
             </span>
          )}
        </div>
      ))}
    </div>
  );
};

export default Breadcrumbs;
