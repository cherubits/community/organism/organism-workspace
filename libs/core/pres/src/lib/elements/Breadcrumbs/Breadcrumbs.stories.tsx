import type { Meta, StoryObj } from '@storybook/react';
import { Breadcrumbs } from './Breadcrumbs';

import { userEvent, within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof Breadcrumbs> = {
  component: Breadcrumbs,
  title: 'Elements/Breadcrumbs',
};
export default meta;
type Story = StoryObj<typeof Breadcrumbs>;

export const Primary: Story = {
  args: {
    items: [
      {
        title: 'Title #1',
        link: '#'
      },
      {
        title: 'Title #2',
        link: '#'
      },
      {
        title: 'Title #3',
        link: '#'
      },
      {
        title: 'Title #4',
      }
    ]
  },
};

export const Secondary: Story = {
  args: {
    items: [
      {
        title: 'Title #1',
        link: '#'
      },
      {
        title: 'Title #2',
        link: '#'
      },
      {
        title: 'Title #3',
        link: '#'
      },
      {
        title: 'Title #4',
      }
    ]
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Title #1/gi)).toBeTruthy();
    expect(canvas.getByText(/Title #2/gi)).toBeTruthy();
    expect(canvas.getByText(/Title #3/gi)).toBeTruthy();
    expect(canvas.getByText(/Title #4/gi)).toBeTruthy();


  },
};
