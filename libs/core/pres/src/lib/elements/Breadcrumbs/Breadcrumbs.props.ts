import { ReactNode } from "react";

export interface BreadcrumbProps {
  title: string;
  link?: string;
}

export interface BreadcrumbsProps {
  items: Array<BreadcrumbProps>;
  className: string;
  children?: ReactNode;
}
