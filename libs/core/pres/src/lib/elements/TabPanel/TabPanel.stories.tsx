import type { Meta, StoryObj } from '@storybook/react';
import { TabPanel } from './TabPanel';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof TabPanel> = {
  component: TabPanel,
  title: 'TabPanel',
};
export default meta;
type Story = StoryObj<typeof TabPanel>;

export const Primary = {
  args: {
    index: 0,
    value: 1,
    children: []
  },
};

export const Secondary: Story = {
  args: {
    index: 0,
    value: 1,
    children: []
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to Tabpanel!/gi)).toBeTruthy();
  },
};
