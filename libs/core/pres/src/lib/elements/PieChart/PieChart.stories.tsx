import type { Meta, StoryObj } from '@storybook/react';
import { PieChart } from './PieChart';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof PieChart> = {
  component: PieChart,
  title: 'PieChart',
};
export default meta;
type Story = StoryObj<typeof PieChart>;

export const Primary = {
  args: {
    height: 450,
    width: 450,
    margin: 40,
    data: [
      { label: 'a', value: 8 },
      { label: 'b', value: 2 },
      { label: 'c', value: 1 },
    ],
    labelFn: (d: any) => d.label,
    valueFn: (d: any) => d.value,
  },
};

export const Heading: Story = {
  args: {
    height: 320,
    width: 320,
    margin: 40,
    data: [
      { label: 'carbs', value: 12 },
      { label: 'protein', value: 33 },
      { label: 'fat', value: 55 },
    ],
    labelFn: (d: any) => d.label,
    valueFn: (d: any) => d.value,
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to PieChart!/gi)).toBeTruthy();
  },
};
