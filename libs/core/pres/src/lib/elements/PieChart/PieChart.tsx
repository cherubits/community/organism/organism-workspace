import { FC, useEffect, useRef } from 'react';
import { PieChartProps } from './PieChart.props';
import { DefaultArcObject, PieArcDatum, ValueFn, scaleOrdinal, select, pie, schemeSet2, arc, Arc } from 'd3';

interface Datum {
  data: string;
  value: number;
}


export const PieChart: FC<PieChartProps> = ({
  height = 450,
  width = 450,
  margin = 40,
  data = { a: 9, b: 20, c: 30, d: 8, e: 12 },
  labelFn,
  valueFn,
}: PieChartProps) => {
  const ref = useRef(null);
  // const fullConfig = resolveConfig(tailwindConfig);

  useEffect(() => {
    const svgElement = select(ref.current);
    const radius = Math.min(width, height) / 2 - margin;
    svgElement.selectAll('*').remove();
    const graphics = svgElement
      .attr('width', width)
      .attr('height', height)
      .append('g');
    graphics.attr(
      'transform',
      'translate(' + width / 2 + ',' + height / 2 + ')'
    );

    // set the color scale
    const color = scaleOrdinal().domain(data).range(schemeSet2);
    const p = pie<Datum>().value(valueFn);
    const data_ready = p(data);
    // Now I know that group A goes from 0 degrees to x degrees and so on.
    console.log('data ready', data_ready);
    const a = arc<PieArcDatum<Datum>>();

    // shape helper to build arcs:
    const arcGenerator = a.innerRadius(0).outerRadius(radius);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    graphics
      .selectAll('mySlices')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', arcGenerator)
      .attr('fill', (d: any) => {
        return `${color(d.data.label)}`;
      })
      .attr('stroke', 'black')
      .style('stroke-width', '1px')
      .style('opacity', 0.7);

    graphics
      .selectAll('mySlices')
      .data(data_ready)
      .enter()
      .append('text')
      .text(labelFn)
      .attr('transform', (d: any) => {
        return 'translate(' + arcGenerator.centroid(d) + ')';
      })
      .style('text-anchor', 'middle')
      .style('font-size', 17);
  }, [data]);

  return <svg viewBox={`0 0 ${height} ${width}`} ref={ref} />;
};

export default PieChart;
