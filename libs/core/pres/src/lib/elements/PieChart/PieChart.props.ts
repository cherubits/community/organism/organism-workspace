import { InternSet } from 'd3';

/* eslint-disable-next-line */
export interface PieChartProps {
  // given d in data, returns the (ordinal) label
  // name: any;
  // // given d in data, returns the (quantitative) value
  // value: any;
  // title: string;
  width: number;
  height: number;
  margin: number;
  // innerRadius: number;
  // outerRadius: number;
  // labelRadius: number;
  // format: string;
  // names: InternSet<unknown>;
  // colors: readonly string[];
  // stroke: string|number;
  // strokeWidth: number;
  // strokeLinejoin: string;
  // padAngle: string|number;
  data: any;
  labelFn: (a: any) => any;
  valueFn: (a: any) => number;
}

// export interface Arc {
//   path: string
//   data: T
//   color: string
// }
