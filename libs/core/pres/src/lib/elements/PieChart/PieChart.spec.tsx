import { render } from '@testing-library/react';

import PieChart from './PieChart';

describe('PieChart', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <PieChart
        height={200}
        width={200}
        labelFn={(d) => d.label}
        margin={20}
        valueFn={(d) => d.value}
        data={[
          { label: 'a', value: 8 },
          { label: 'b', value: 2 },
          { label: 'c', value: 1 },
        ]}
      />
    );
    expect(baseElement).toBeTruthy();
  });
});
