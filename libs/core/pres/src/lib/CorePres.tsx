import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface CorePresProps {}

const StyledCorePres = styled.div`
  color: pink;
`;

export function CorePres(props: CorePresProps) {
  return (
    <StyledCorePres>
      <h1>Welcome to CorePres!</h1>
    </StyledCorePres>
  );
}

export default CorePres;
