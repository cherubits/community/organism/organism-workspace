import type { Meta, StoryObj } from '@storybook/react';
import { CorePres } from './CorePres';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof CorePres> = {
  component: CorePres,
  title: 'CorePres',
};
export default meta;
type Story = StoryObj<typeof CorePres>;

export const Primary = {
  args: {},
};

export const Heading: Story = {
  args: {},
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to CorePres!/gi)).toBeTruthy();
  },
};
