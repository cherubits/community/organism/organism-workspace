import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import MenuIcon from '@mui/icons-material/Menu';

import { AppBarProps } from './AppBar.props';
import { IconButton, Toolbar, Typography, styled } from '@mui/material';
import { FC, MouseEventHandler, ReactNode } from 'react';
/* eslint-disable-next-line */
export interface AppHeaderProps extends AppBarProps {
  handleDrawerOpen: MouseEventHandler<HTMLButtonElement>;
  children?: ReactNode | ReactNode[];
}

const StyledAppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open, drawerWidth }) => ({
  zIndex: theme.zIndex.drawer + 10,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));


export const AppHeader: FC<AppHeaderProps> = ({
  handleDrawerOpen,
  open,
  drawerWidth,
  children,
}: AppHeaderProps) => {
  return (
      <StyledAppBar open={open} drawerWidth={drawerWidth}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          {children}
        </Toolbar>
      </StyledAppBar>
  );
};

export default AppHeader;
