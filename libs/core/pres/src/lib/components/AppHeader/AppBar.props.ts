import { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import { MouseEventHandler } from 'react';

export interface AppBarProps {
  open: boolean;
  drawerWidth: number;
}
