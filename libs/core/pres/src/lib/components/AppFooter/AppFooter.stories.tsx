import type { Meta, StoryObj } from '@storybook/react';
import { AppFooter } from './AppFooter';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof AppFooter> = {
  component: AppFooter,
  title: 'Components/AppFooter',
};
export default meta;
type Story = StoryObj<typeof AppFooter>;

export const Primary = {
  args: {},
};

export const Heading: Story = {
  args: {},
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to AppFooter!/gi)).toBeTruthy();
  },
};
