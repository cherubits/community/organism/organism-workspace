import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface AppFooterProps {}

const StyledAppFooter = styled.div`
  color: pink;
`;

export function AppFooter(props: AppFooterProps) {
  return (
    <StyledAppFooter>
      <h1>Welcome to AppFooter!</h1>
    </StyledAppFooter>
  );
}

export default AppFooter;
