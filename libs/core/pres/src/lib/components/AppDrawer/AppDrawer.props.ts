import { DrawerProps as MuiDrawerProps } from '@mui/material/Drawer';
import { MouseEventHandler, ReactNode } from 'react';

export interface AppDrawerProps {
  open: boolean;
  drawerWidth: number;
  handleDrawerClose: MouseEventHandler<HTMLButtonElement>;
  children?: ReactNode | ReactNode[];
 }
