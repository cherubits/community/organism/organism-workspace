import type { Meta, StoryObj } from '@storybook/react';
import { AppDrawer } from './AppDrawer';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';
import {
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Divider,
} from '@mui/material';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';

const meta: Meta<typeof AppDrawer> = {
  component: AppDrawer,
  title: 'Components/AppDrawer',
};
export default meta;
type Story = StoryObj<typeof AppDrawer>;
const open = true;

export const Primary: Story = {
  args: {
    drawerWidth: 200,
    handleDrawerClose: () => console.log('drawer close'),
    open: true,
    children: [
      <List key="a">
        {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
          <ListItem key={text} disablePadding sx={{ display: 'block' }}>
            <ListItemButton
              sx={{
                minHeight: 48,
                justifyContent: open ? 'initial' : 'center',
                px: 2.5,
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: open ? 3 : 'auto',
                  justifyContent: 'center',
                }}
              >
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} sx={{ opacity: open ? 1 : 0 }} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>,
      <Divider key="b" />,
      <List  key="c">
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
          <ListItem key={text} disablePadding sx={{ display: 'block' }}>
            <ListItemButton
              sx={{
                minHeight: 48,
                justifyContent: open ? 'initial' : 'center',
                px: 2.5,
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: open ? 3 : 'auto',
                  justifyContent: 'center',
                }}
              >
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} sx={{ opacity: open ? 1 : 0 }} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>,
    ],
  },
};

export const Heading: Story = {
  args: {
    drawerWidth: 200,
    handleDrawerClose: () => console.log('drawer close'),
    open: true,
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to AppDrawer!/gi)).toBeTruthy();
  },
};
