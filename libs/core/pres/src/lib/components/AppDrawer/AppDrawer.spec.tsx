import { render } from '@testing-library/react';

import AppDrawer from './AppDrawer';

describe('AppDrawer', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <AppDrawer
        drawerWidth={200}
        handleDrawerClose={() => console.log('Drawer close')}
        open={false}
      />
    );
    expect(baseElement).toBeTruthy();
  });
});
