import { ReactNode } from "react";

export interface Menu {
  text: string;
  icon?: ReactNode;
}
