import { render } from '@testing-library/react';

import BasicAppLayout from './BasicAppLayout';

describe('BasicAppLayout', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<BasicAppLayout />);
    expect(baseElement).toBeTruthy();
  });
});
