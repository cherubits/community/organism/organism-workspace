import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface BasicAppLayoutProps {}

export function BasicAppLayout(props: BasicAppLayoutProps) {
  const StyledBasicAppLayout = styled.div`
    color: pink;
  `;

  return (
    <StyledBasicAppLayout>
      <h1>Welcome to BasicAppLayout!</h1>
    </StyledBasicAppLayout>
  );
}

export default BasicAppLayout;
