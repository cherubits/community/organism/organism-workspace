import type { Meta, StoryObj } from '@storybook/react';
import { BasicAppLayout } from './BasicAppLayout';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof BasicAppLayout> = {
  component: BasicAppLayout,
  title: 'Layout/BasicAppLayout',
};
export default meta;
type Story = StoryObj<typeof BasicAppLayout>;

export const Primary = {
  args: {},
};

export const Heading: Story = {
  args: {},
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to BasicAppLayout!/gi)).toBeTruthy();
  },
};
