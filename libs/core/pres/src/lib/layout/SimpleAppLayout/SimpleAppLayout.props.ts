import { ReactNode } from "react";
import {Menu} from '../../properties/Menu';
export interface SimpleAppLayoutProps {
  drawerWidth: number;
  open: boolean;
  menus?: Menu[];
  children?: ReactNode | ReactNode[];
}
