import {
  Box,
  CssBaseline,
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@mui/material';
import AppHeader from '../../components/AppHeader/AppHeader';
import { FC, useState } from 'react';
import AppDrawer from '../../components/AppDrawer/AppDrawer';
import { DrawerHeader } from '../../components/DrawerHeader/DrawerHeader';
import { SimpleAppLayoutProps } from './SimpleAppLayout.props';
import { Menu } from '../../properties/Menu';

export const SimpleAppLayout: FC<SimpleAppLayoutProps> = ({
  open,
  drawerWidth,
  menus,
  children,
}: SimpleAppLayoutProps) => {
  const [opened, setOpen] = useState(open);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppHeader
        drawerWidth={drawerWidth}
        handleDrawerOpen={handleDrawerOpen}
        open={opened}
      >
        <Typography variant="h6" noWrap component="div">
          Mini variant drawer
        </Typography>
      </AppHeader>
      <AppDrawer
        drawerWidth={drawerWidth}
        handleDrawerClose={handleDrawerClose}
        open={opened}
      >
        <List>
          {menus?.map((menu: Menu, index: number) => (
            <ListItem key={menu.text} disablePadding sx={{ display: 'block' }}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: open ? 'initial' : 'center',
                  px: 2.5,
                }}
              >
                <ListItemIcon
                  sx={{
                    minWidth: 0,
                    mr: open ? 3 : 'auto',
                    justifyContent: 'center',
                  }}
                >
                  {menu.icon}
                </ListItemIcon>
                <ListItemText
                  primary={menu.text}
                  sx={{ opacity: open ? 1 : 0 }}
                />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </AppDrawer>
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        {children}
      </Box>
    </Box>
  );
};

export default SimpleAppLayout;
