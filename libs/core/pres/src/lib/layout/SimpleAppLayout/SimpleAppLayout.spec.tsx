import { render } from '@testing-library/react';

import SimpleAppLayout from './SimpleAppLayout';

describe('SimpleAppLayout', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SimpleAppLayout />);
    expect(baseElement).toBeTruthy();
  });
});
