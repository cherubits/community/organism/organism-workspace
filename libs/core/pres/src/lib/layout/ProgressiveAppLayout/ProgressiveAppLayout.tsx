import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface ProgressiveAppLayoutProps {}

const StyledProgressiveAppLayout = styled.div`
  color: pink;
`;

export function ProgressiveAppLayout(props: ProgressiveAppLayoutProps) {
  return (
    <StyledProgressiveAppLayout>
      <h1>Welcome to ProgressiveAppLayout!</h1>
    </StyledProgressiveAppLayout>
  );
}

export default ProgressiveAppLayout;
