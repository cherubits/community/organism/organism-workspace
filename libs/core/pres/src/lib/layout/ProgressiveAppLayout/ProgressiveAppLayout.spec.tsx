import { render } from '@testing-library/react';

import ProgressiveAppLayout from './ProgressiveAppLayout';

describe('ProgressiveAppLayout', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ProgressiveAppLayout />);
    expect(baseElement).toBeTruthy();
  });
});
