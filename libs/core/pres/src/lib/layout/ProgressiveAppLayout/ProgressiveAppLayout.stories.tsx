import type { Meta, StoryObj } from '@storybook/react';
import { ProgressiveAppLayout } from './ProgressiveAppLayout';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof ProgressiveAppLayout> = {
  component: ProgressiveAppLayout,
  title: 'Layout/ProgressiveAppLayout',
};
export default meta;
type Story = StoryObj<typeof ProgressiveAppLayout>;

export const Primary = {
  args: {},
};

export const Heading: Story = {
  args: {},
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to ProgressiveAppLayout!/gi)).toBeTruthy();
  },
};
