# schef-app

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build schef-app` to build the library.

## Running unit tests

Run `nx test schef-app` to execute the unit tests via [Jest](https://jestjs.io).
