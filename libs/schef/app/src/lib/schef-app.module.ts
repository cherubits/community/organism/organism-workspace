import { Module } from '@nestjs/common';
import { SchefAppService } from './schef-app.service';

@Module({
  controllers: [],
  providers: [SchefAppService],
  exports: [SchefAppService],
})
export class SchefAppModule {}
