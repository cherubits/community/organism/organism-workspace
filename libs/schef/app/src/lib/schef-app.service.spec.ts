import { Test } from '@nestjs/testing';
import { SchefAppService } from './schef-app.service';

describe('SchefAppService', () => {
  let service: SchefAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SchefAppService],
    }).compile();

    service = module.get(SchefAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
