# schef-common

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build schef-common` to build the library.

## Running unit tests

Run `nx test schef-common` to execute the unit tests via [Jest](https://jestjs.io).
