import { Module } from '@nestjs/common';
import { SchefInfraService } from './schef-infra.service';

@Module({
  controllers: [],
  providers: [SchefInfraService],
  exports: [SchefInfraService],
})
export class SchefInfraModule {}
