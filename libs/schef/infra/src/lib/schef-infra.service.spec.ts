import { Test } from '@nestjs/testing';
import { SchefInfraService } from './schef-infra.service';

describe('SchefInfraService', () => {
  let service: SchefInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SchefInfraService],
    }).compile();

    service = module.get(SchefInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
