import { schefClient } from './schef-client';

describe('schefClient', () => {
  it('should work', () => {
    expect(schefClient()).toEqual('schef-client');
  });
});
