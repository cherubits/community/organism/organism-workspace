import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface SchefPresProps {}

const StyledSchefPres = styled.div`
  color: pink;
`;

export function SchefPres(props: SchefPresProps) {
  return (
    <StyledSchefPres>
      <h1>Welcome to SchefPres!</h1>
    </StyledSchefPres>
  );
}

export default SchefPres;
