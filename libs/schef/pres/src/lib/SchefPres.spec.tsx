import { render } from '@testing-library/react';

import SchefPres from './SchefPres';

describe('SchefPres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SchefPres />);
    expect(baseElement).toBeTruthy();
  });
});
