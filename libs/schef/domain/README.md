# schef-domain

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build schef-domain` to build the library.

## Running unit tests

Run `nx test schef-domain` to execute the unit tests via [Jest](https://jestjs.io).
