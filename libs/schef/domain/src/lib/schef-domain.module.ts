import { Module } from '@nestjs/common';
import { SchefDomainService } from './schef-domain.service';

@Module({
  controllers: [],
  providers: [SchefDomainService],
  exports: [SchefDomainService],
})
export class SchefDomainModule {}
