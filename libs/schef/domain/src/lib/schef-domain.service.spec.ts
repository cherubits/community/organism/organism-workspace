import { Test } from '@nestjs/testing';
import { SchefDomainService } from './schef-domain.service';

describe('SchefDomainService', () => {
  let service: SchefDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SchefDomainService],
    }).compile();

    service = module.get(SchefDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
