export interface Value {
  value: number;
  unit: string;
}

export interface Ingredient {
  id: string;
  title: string;

  carbs: number;
  fat: number;
  protein: number;
  fiber: number;
  energy: number;

  mass: Value;
}

export interface Task {
  id: string;
  title: string;
  description?: string;
  occurrence?: number;

}
