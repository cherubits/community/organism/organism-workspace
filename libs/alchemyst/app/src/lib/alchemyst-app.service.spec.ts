import { Test } from '@nestjs/testing';
import { AlchemystAppService } from './alchemyst-app.service';

describe('AlchemystAppService', () => {
  let service: AlchemystAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [AlchemystAppService],
    }).compile();

    service = module.get(AlchemystAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
