import { alchemystClient } from './alchemyst-client';

describe('alchemystClient', () => {
  it('should work', () => {
    expect(alchemystClient()).toEqual('alchemyst-client');
  });
});
