import { Test } from '@nestjs/testing';
import { AlchemystInfraService } from './alchemyst-infra.service';

describe('AlchemystInfraService', () => {
  let service: AlchemystInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [AlchemystInfraService],
    }).compile();

    service = module.get(AlchemystInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
