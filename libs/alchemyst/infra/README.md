# alchemyst-infra

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build alchemyst-infra` to build the library.

## Running unit tests

Run `nx test alchemyst-infra` to execute the unit tests via [Jest](https://jestjs.io).
