import { Test } from '@nestjs/testing';
import { AlchemystDomainService } from './alchemyst-domain.service';

describe('AlchemystDomainService', () => {
  let service: AlchemystDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [AlchemystDomainService],
    }).compile();

    service = module.get(AlchemystDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
