import { Module } from '@nestjs/common';
import { AlchemystDomainService } from './alchemyst-domain.service';

@Module({
  controllers: [],
  providers: [AlchemystDomainService],
  exports: [AlchemystDomainService],
})
export class AlchemystDomainModule {}
