import { render } from '@testing-library/react';

import AlchemystPres from './AlchemystPres';

describe('AlchemystPres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlchemystPres />);
    expect(baseElement).toBeTruthy();
  });
});
