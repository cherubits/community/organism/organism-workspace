import type { Meta, StoryObj } from '@storybook/react';
import { RecipeEditor } from './RecipeEditor';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof RecipeEditor> = {
  component: RecipeEditor,
  title: 'RecipeEditorPage',
};
export default meta;
type Story = StoryObj<typeof RecipeEditor>;

export const Primary = {
  args: {},
};

export const Secondary: Story = {
  args: {},
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to RecipeEditorPage!/gi)).toBeTruthy();
  },
};
