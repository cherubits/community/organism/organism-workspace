import { render } from '@testing-library/react';

import RecipeEditor from './RecipeEditor';

describe('RecipeEditor', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<RecipeEditor />);
    expect(baseElement).toBeTruthy();
  });
});
