import styled from '@emotion/styled';
import { FC } from 'react';
import { Formik, Form, Field, ErrorMessage, useFormik } from 'formik';
import { useTranslation } from 'react-i18next';
import { object, string, number, date, InferType } from 'yup';
import { TextField, Button } from '@mui/material';

const $material = object({
  label: string().required(),
  description: string().nullable(),
  author: string().email(),
  website: string().url().nullable(),
  createdOn: date().default(() => new Date()),
});

/* eslint-disable-next-line */
export interface MaterialEditorProps {
  author: string;
}

const StyledMaterialEditor = styled.div`
  color: pink;
`;

export const MaterialEditor: FC<MaterialEditorProps> = ({
  author,
}: MaterialEditorProps) => {
  const formik = useFormik({
    initialValues: {
      label: 'tojás',
      description: 'tojás leírás',
      author,
      website: '',
    },
    validationSchema: $material,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });
  const { t, i18n } = useTranslation();

  return (
    <StyledMaterialEditor>
      <form onSubmit={formik.handleSubmit}>
        <TextField
          fullWidth
          id="label"
          name="label"
          label={t("Label", { ns: 'common' })}
          type="text"
          value={formik.values.label}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.label && Boolean(formik.errors.label)}
          helperText={formik.touched.label && formik.errors.label}
          variant="filled"
        />

        <TextField
          fullWidth
          id="description"
          name="description"
          label="description"
          multiline
          maxRows={4}
          value={formik.values.description}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={
            formik.touched.description && Boolean(formik.errors.description)
          }
          helperText={formik.touched.description && formik.errors.description}
          variant="filled"
        />

        <Button color="primary" variant="contained" fullWidth type="submit">
          Submit
        </Button>
      </form>
    </StyledMaterialEditor>
  );
};

export default MaterialEditor;
