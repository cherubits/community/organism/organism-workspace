import { render } from '@testing-library/react';

import MaterialEditor from './MaterialEditor';

describe('MaterialEditor', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<MaterialEditor />);
    expect(baseElement).toBeTruthy();
  });
});
