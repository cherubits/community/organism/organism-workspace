import { FC, useState } from 'react';
import {
  TableCell,
  TableRow, TextField,
  InputAdornment
} from '@mui/material';
import { IngredientProps } from './Ingredient.props';


export const IngredientRow: FC<IngredientProps> = ({ ingredient, handleDataUpdate }: IngredientProps) => {
  const [title, setTitle] = useState<string>(ingredient.title);
  const [energy, setEnergy] = useState<number>(ingredient.energy * ingredient.mass.value);
  const [carbs, setCarbs] = useState<number>(ingredient.carbs * ingredient.mass.value);
  const [fat, setFat] = useState<number>(ingredient.fat * ingredient.mass.value);
  const [fiber, setFiber] = useState<number>(ingredient.fiber * ingredient.mass.value);
  const [protein, setProtein] = useState<number>(ingredient.protein * ingredient.mass.value);
  const [mass, setMass] = useState<number>(ingredient.mass.value * ingredient.mass.value);

  const onChangeMass = (event: React.ChangeEvent<HTMLInputElement>) => {
    const v = parseFloat(event.target.value);
    setMass(v);
    setCarbs(ingredient.carbs * v);
    setProtein(ingredient.protein * v);
    setFat(ingredient.fat * v);
    setFiber(ingredient.fiber * v);
    setEnergy(ingredient.energy * v);
    const i = {
      ...ingredient,
      energy,
      mass: { value: v, unit: ingredient.mass.unit },
      carbs,
      fat,
      fiber,
      protein
    };
    handleDataUpdate?.(i);
    console.log('mass changed to', i);
  };

  return (
    <TableRow key={`row-${ingredient.id}`}>
      <TableCell>
        <label>{title}</label>
      </TableCell>
      <TableCell>
        <span>{energy.toFixed(2)}</span>
      </TableCell>
      <TableCell>
        <span>{carbs.toFixed(2)}</span>
      </TableCell>
      <TableCell>
        <span>{fat.toFixed(2)}</span>
      </TableCell>
      <TableCell>
        <span>{protein.toFixed(2)}</span>
      </TableCell>
      <TableCell>
        <span>{fiber.toFixed(2)}</span>
      </TableCell>
      <TableCell>
        <TextField
          type="number"
          size={'small'}
          value={mass.toFixed(2)}
          onChange={onChangeMass}
          InputProps={{
            endAdornment: <InputAdornment position="end">g</InputAdornment>,
          }} />
      </TableCell>
    </TableRow>
  );
};
