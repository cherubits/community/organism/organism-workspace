import { Ingredient } from '@organism/alchemyst-common';

export interface IngredientProps {
  ingredient: Ingredient;
  handleDataUpdate?: (...ingredients: Ingredient[]) => void;
}
