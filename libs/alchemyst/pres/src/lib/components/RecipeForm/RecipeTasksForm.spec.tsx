import { render } from '@testing-library/react';

import RecipeTasksForm from './RecipeTasksForm';

describe('RecipeTasksForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<RecipeTasksForm />);
    expect(baseElement).toBeTruthy();
  });
});
