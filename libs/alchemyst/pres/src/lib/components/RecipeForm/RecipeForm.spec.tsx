import { render } from '@testing-library/react';

import RecipeForm from './RecipeForm';

describe('RecipeForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<RecipeForm ingredients={[]} title='' />);
    expect(baseElement).toBeTruthy();
  });
});
