import { FC, useEffect, useState } from 'react';
import { RecipeFormProps } from './RecipeForm.props';
import {
  Table,
  TableBody,
  TableHead,
  TableCell,
  TableContainer,
  TableRow, TableFooter
} from '@mui/material';
import { useTranslation } from 'react-i18next';
import { summary } from './RecipeForm';
import { IngredientRow } from './IngredientRow';

export const RecipeIngredientsForm: FC<RecipeFormProps> = ({
  ingredients, handleDataUpdate
}: RecipeFormProps) => {
  const { t } = useTranslation();
  const [energy, setEnergy] = useState<number>(summary(ingredients, 'energy'));
  const [carbs, setCarbs] = useState<number>(summary(ingredients, 'carbs'));
  const [fat, setFat] = useState<number>(summary(ingredients, 'fat'));
  const [fiber, setFiber] = useState<number>(summary(ingredients, 'fiber'));
  const [protein, setProtein] = useState<number>(summary(ingredients, 'protein'));
  const [mass, setMass] = useState<number>(summary(ingredients, 'mass'));

  useEffect(() => {
    setCarbs(summary(ingredients, 'energy'));
    setCarbs(summary(ingredients, 'carbs'));
    setProtein(summary(ingredients, 'protein'));
    setFat(summary(ingredients, 'fat'));
    setFiber(summary(ingredients, 'fiber'));
  }, [ingredients]);

  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>{t('name')}</TableCell>
            <TableCell>{t('energy')}</TableCell>
            <TableCell>{t('carbs')}</TableCell>
            <TableCell>{t('protein')}</TableCell>
            <TableCell>{t('fat')}</TableCell>
            <TableCell>{t('fiber')}</TableCell>
            <TableCell>{t('mass')}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {ingredients.map((ingredient) => (
            <IngredientRow ingredient={ingredient} handleDataUpdate={handleDataUpdate} key={`ingredient-${ingredient.id}`} />
          ))}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TableCell>{t('summary')}</TableCell>
            <TableCell>{energy.toFixed(2)}</TableCell>
            <TableCell>{carbs.toFixed(2)}</TableCell>
            <TableCell>{protein.toFixed(2)}</TableCell>
            <TableCell>{fat.toFixed(2)}</TableCell>
            <TableCell>{fiber.toFixed(2)}</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
};
