import type { Meta, StoryObj } from '@storybook/react';
import { RecipeForm } from './RecipeForm';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';
import { faker } from '@faker-js/faker';

const meta: Meta<typeof RecipeForm> = {
  component: RecipeForm,
  title: 'RecipeForm',
};
export default meta;
type Story = StoryObj<typeof RecipeForm>;

const maxValue = 0.1;

export const Primary = {
  args: {
    ingredients: [
      {
        id: '1',
        title: 'Padlizsán',
        carbs: faker.number.float(maxValue),
        fat: faker.number.float(maxValue),
        protein: faker.number.float(maxValue),
        fiber: faker.number.float(maxValue),
        energy: faker.number.float(maxValue),
        mass: {
          value: maxValue,
          unit: 'g'
        }
      },
      {
        id: '2',
        title: 'Kaliforniai paprika',
        carbs: faker.number.float(maxValue),
        fat: faker.number.float(maxValue),
        protein: faker.number.float(maxValue),
        fiber: faker.number.float(maxValue),
        energy: faker.number.float(maxValue),
        mass: {
          value: maxValue,
          unit: 'g'
        }
      },
      {
        id: '3',
        title: 'Hagyma',
        carbs: faker.number.float(maxValue),
        fat: faker.number.float(maxValue),
        protein: faker.number.float(maxValue),
        fiber: faker.number.float(maxValue),
        energy: faker.number.float(maxValue),
        mass: {
          value: maxValue,
          unit: 'g'
        }
      }
    ]
  },
};

export const Secondary: Story = {
  args: {},
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to RecipeForm!/gi)).toBeTruthy();
  },
};
