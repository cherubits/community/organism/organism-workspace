import { FC, ReactNode, useEffect, useState, useSyncExternalStore } from 'react';
import { RecipeFormProps } from './RecipeForm.props';
import {
  Paper,
  TablePagination,
  Box,
  Tab,
  Tabs,
  Typography,
  FormControlLabel,
  Radio,
  RadioGroup,
} from '@mui/material';
import { Ingredient } from '@organism/alchemyst-common';
import { Order } from '@organism/core-common';
import { PieChart, TabPanel } from '@organism/core-pres';
import clsx from 'clsx';
import { useTranslation } from 'react-i18next';
import i18next from './../../../i18n';
import { RecipeDetailsForm } from './RecipeDetailsForm';
import { RecipeIngredientsForm } from './RecipeIngredientsForm';

function a11yTabProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export const summary = (array: Array<Ingredient>, key: string) => {
  const s = array.reduce(
    (accumulator, current) => accumulator + Reflect.get(current, key),
    0
  );
  console.log('sdfsf', key, s);
  return s;
};



export const RecipeForm: FC<RecipeFormProps> = (props: RecipeFormProps) => {
  const { t, i18n } = useTranslation();
  const [ingredients, setIngredients] = useState<Array<Ingredient>>(props.ingredients);
  const [tabIndex, setTabIndex] = useState(0);
  const [order, setOrder] = useState<Order>(Order.ASC);
  const [orderBy, setOrderBy] = useState<keyof Ingredient>('energy');
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = useState(0);

  const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabIndex(newValue);
  };



  const [carbs, setCarbs] = useState(0);
  const [protein, setProtein] = useState(0);
  const [fat, setFat] = useState(0);

  const pieChartData = () => {
    const d = [
      { label: 'carbs', value: carbs },
      { label: 'protein', value: protein },
      { label: 'fat', value: fat },
    ];
    console.log('piechat data', d);
    return d;
  };

  const [pieChart, setPieChart] = useState(pieChartData());

  useEffect(() => {
    setCarbs(summary(ingredients, 'carbs'));
    setProtein(summary(ingredients, 'protein'));
    setFat(summary(ingredients, 'fat'));
    setPieChart(pieChartData());
  }, [ingredients]);

  useEffect(() => {
    setPieChart(pieChartData());
  }, [carbs, protein, fat]);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Ingredient
  ) => {
    const isAsc = orderBy === property && order === Order.ASC;
    setOrder(isAsc ? Order.DESC : Order.ASC);
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleDataUpdate = (...params: Ingredient[]) => {
    setIngredients(ingredients.map((i) => {
      const u = params.filter((a) => a.id === i.id);
      if (u && u.length === 1) {
        return u[0];
      } else {
        return i;
      }
    }));
    // setCarbs(summary(ingredients, 'carbs'));
    // setProtein(summary(ingredients, 'protein'));
    // setFat(summary(ingredients, 'fat'));
    // setPieChart(pieChartData());
    console.log('redraw chart');
  }

  return (
    <div className="flex flex-row shadow-[0_2px_10px] shadow-blackA2">
      <div className="flex flex-wrap items-center gap-[15px] px-5">
        <PieChart
          data={pieChart}
          width={360}
          height={360}
          labelFn={(d: any) => `${d.data.label}: ${d.data.value.toFixed(2)}`}
          margin={40}
          valueFn={(a) => a.value}
        />
      </div>
      <div className="flex flex-col">
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs
            value={tabIndex}
            onChange={handleTabChange}
            aria-label={t('recipe-tabs')}
          >
            <Tab label={t('details')} {...a11yTabProps(0)} />
            <Tab label={t('ingredients')} {...a11yTabProps(1)} />
            <Tab label={t('steps')} {...a11yTabProps(2)} />
          </Tabs>
        </Box>
        <TabPanel index={0} value={tabIndex}>
          {<RecipeDetailsForm handleDataUpdate={handleDataUpdate} ingredients={ingredients} title={props.title} />}
        </TabPanel>
        <TabPanel index={1} value={tabIndex}>
          {<RecipeIngredientsForm handleDataUpdate={handleDataUpdate} ingredients={ingredients} title={props.title} />}
        </TabPanel>
        <TabPanel index={2} value={tabIndex}>
          Item Three
        </TabPanel>
      </div>
    </div>
  );
};

export default RecipeForm;
