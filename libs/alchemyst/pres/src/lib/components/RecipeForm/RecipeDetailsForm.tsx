import { FC } from 'react';
import { RecipeFormProps } from './RecipeForm.props';
import { TextField, Box, FormControl, FormLabel } from '@mui/material';
import { useTranslation } from 'react-i18next';

export const RecipeDetailsForm: FC<RecipeFormProps> = ({
  title, description,
}: RecipeFormProps) => {
  const { t, i18n } = useTranslation();
  return (
    <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
      className="flex flex-col"
    >
      <FormControl className="m-4">
        <FormLabel id="title-label"></FormLabel>
        <TextField
          required
          aria-labelledby="title-label"
          id="outlined-required"
          label={t('Title')}
          placeholder={t('Type recipe title here')}
          value={title}
          error
          helperText={t('Incorrect entry')} />
      </FormControl>
      <FormControl className="m-4">
        <FormLabel id="description-label"></FormLabel>
        <TextField
          minRows={3}
          maxRows={8}
          multiline
          aria-labelledby="description-label"
          id="outlined-required"
          label={t('Description')}
          placeholder={t('Type long description here')}
          value={description} />
      </FormControl>
    </Box>
  );
};
