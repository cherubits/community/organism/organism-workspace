/* eslint-disable-next-line */

import { Ingredient } from '@organism/alchemyst-common';

export interface RecipeFormProps {
  title: string;
  description?: string;
  ingredients: Array<Ingredient>;
  handleDataUpdate?: (...ingredient: Ingredient[]) => void;
}
