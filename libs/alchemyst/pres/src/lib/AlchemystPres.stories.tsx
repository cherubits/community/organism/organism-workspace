import type { Meta, StoryObj } from '@storybook/react';
import { AlchemystPres } from './AlchemystPres';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof AlchemystPres> = {
  component: AlchemystPres,
  title: 'AlchemystPres',
};
export default meta;
type Story = StoryObj<typeof AlchemystPres>;

export const Primary = {
  args: {},
};

export const Heading: Story = {
  args: {},
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Welcome to AlchemystPres!/gi)).toBeTruthy();
  },
};
