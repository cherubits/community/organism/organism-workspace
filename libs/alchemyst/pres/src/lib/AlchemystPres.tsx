import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface AlchemystPresProps {}

const StyledAlchemystPres = styled.div`
  color: pink;
`;

export function AlchemystPres(props: AlchemystPresProps) {
  return (
    <StyledAlchemystPres>
      <h1>Welcome to AlchemystPres!</h1>
    </StyledAlchemystPres>
  );
}

export default AlchemystPres;
