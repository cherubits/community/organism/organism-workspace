import $i18n from './i18n';
// Replace your-framework with the framework you are using (e.g., react, vue3)
import { ThemeProvider, createTheme } from '@mui/material';
import { Preview } from '@storybook/react';
import { themes } from '@storybook/theming';
// import React, { Suspense, useEffect } from 'react';
// import { I18nextProvider } from 'react-i18next';
import '../src/styles/tailwind.css'

const theme = createTheme();

const locales = {
  en: { title: 'English', right: 'EN', value: 'en' },
  hu: { title: 'Magyar', right: 'HU', value: 'de' },
};

export const globalTypes = {
  locale: {
    name: 'Locale',
    description: 'Internationalization locale',
    toolbar: {
      icon: 'globe',
      items: [
        { value: 'en', title: 'English' },
        { value: 'hu', title: 'Hungarian' },
      ],
      showName: true,
    },
  },
 };

const preview: Preview = {
  globals: {
    locale: 'en',
    locales: locales,

  },
  parameters: {
    layout: 'centered',
    docs: {
      theme: themes.dark,
    },
    // $i18n,
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
  decorators: [
    (Story) => (
      <ThemeProvider theme={theme}>
        <Story />
      </ThemeProvider>
    ),
    // (Story, context) => {
    //   const { locale } = context.globals;

    //   // When the locale global changes
    //   // Set the new locale in i18n
    //   useEffect(() => {
    //     $i18n.changeLanguage(locale);
    //   }, [locale]);
    //   return (
    //     // This catches the suspense from components not yet ready (still loading translations)
    //     // Alternative: set useSuspense to false on i18next.options.react when initializing i18next
    //     <Suspense fallback={<div>loading translations...</div>}>
    //       <I18nextProvider i18n={$i18n}>
    //         <Story />
    //       </I18nextProvider>
    //     </Suspense>
    //   );
    // },
  ],
};

export default preview;
