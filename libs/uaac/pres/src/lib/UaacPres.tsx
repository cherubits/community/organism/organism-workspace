import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface UaacPresProps {}

const StyledUaacPres = styled.div`
  color: pink;
`;

export function UaacPres(props: UaacPresProps) {
  return (
    <StyledUaacPres>
      <h1>Welcome to UaacPres!</h1>
    </StyledUaacPres>
  );
}

export default UaacPres;
