import { render } from '@testing-library/react';

import UaacPres from './UaacPres';

describe('UaacPres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UaacPres />);
    expect(baseElement).toBeTruthy();
  });
});
