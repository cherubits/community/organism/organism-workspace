import { Module } from '@nestjs/common';
import { UaacInfraService } from './uaac-infra.service';

@Module({
  controllers: [],
  providers: [UaacInfraService],
  exports: [UaacInfraService],
})
export class UaacInfraModule {}
