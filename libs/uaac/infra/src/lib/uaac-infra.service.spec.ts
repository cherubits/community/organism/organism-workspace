import { Test } from '@nestjs/testing';
import { UaacInfraService } from './uaac-infra.service';

describe('UaacInfraService', () => {
  let service: UaacInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [UaacInfraService],
    }).compile();

    service = module.get(UaacInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
