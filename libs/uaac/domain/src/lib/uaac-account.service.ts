import { Injectable } from '@nestjs/common';
import { UaacPrismaService } from './uaac-prisma.service';
import { User, Prisma } from '@prisma/client/uaac';

@Injectable()
export class UaacAccountService {
  constructor(private prisma: UaacPrismaService) {}

  async user(
    userWhereUniqueInput: Prisma.UserWhereUniqueInput,
  ): Promise<User | null> {
    return this.prisma.user.findUnique({
      where: userWhereUniqueInput,
    });
  }
}
