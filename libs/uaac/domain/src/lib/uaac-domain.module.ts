import { Module } from '@nestjs/common';
import { UaacDomainService } from './uaac-domain.service';
import { UaacPrismaService } from './uaac-prisma.service';
import { UaacAccountService } from './uaac-account.service';

@Module({
  controllers: [],
  providers: [UaacDomainService, UaacPrismaService, UaacAccountService],
  exports: [UaacDomainService],
})
export class UaacDomainModule {}
