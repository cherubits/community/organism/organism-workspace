import { Test, TestingModule } from '@nestjs/testing';
import { UaacAccountService } from './uaac-account.service';
import { UaacPrismaService } from './uaac-prisma.service';

describe('UaacAccountService', () => {
  let service: UaacAccountService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers: [UaacAccountService, UaacPrismaService],
    }).compile();

    service = await moduleRef.resolve(UaacAccountService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
