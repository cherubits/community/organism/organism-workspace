import { Test, TestingModule } from '@nestjs/testing';
import { UaacPrismaService } from './uaac-prisma.service';

describe('UaacPrismaService', () => {
  let service: UaacPrismaService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers: [UaacPrismaService],
    }).compile();

    service = await moduleRef.resolve(UaacPrismaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
