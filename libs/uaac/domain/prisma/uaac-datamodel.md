```mermaid
erDiagram

  "users" {
    String id "🗝️"
    Boolean active 
    }
  

  "groups" {
    String id "🗝️"
    }
  

  "roles" {
    String id "🗝️"
    }
  

  "permissions" {
    String id "🗝️"
    }
  
    "users" o{--}o "groups" : "groups"
    "groups" o{--}o "users" : "members"
    "roles" o{--}o "permissions" : "permissions"
    "permissions" o{--}o "roles" : "roles"
```
