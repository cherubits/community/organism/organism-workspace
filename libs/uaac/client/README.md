# uaac-client

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build uaac-client` to build the library.

## Running unit tests

Run `nx test uaac-client` to execute the unit tests via [Jest](https://jestjs.io).
