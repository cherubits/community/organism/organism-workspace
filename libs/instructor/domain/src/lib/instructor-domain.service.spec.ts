import { Test } from '@nestjs/testing';
import { InstructorDomainService } from './instructor-domain.service';

describe('InstructorDomainService', () => {
  let service: InstructorDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [InstructorDomainService],
    }).compile();

    service = module.get(InstructorDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
