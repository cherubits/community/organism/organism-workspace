import { Module } from '@nestjs/common';
import { InstructorDomainService } from './instructor-domain.service';

@Module({
  controllers: [],
  providers: [InstructorDomainService],
  exports: [InstructorDomainService],
})
export class InstructorDomainModule {}
