# instructor-domain

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build instructor-domain` to build the library.

## Running unit tests

Run `nx test instructor-domain` to execute the unit tests via [Jest](https://jestjs.io).
