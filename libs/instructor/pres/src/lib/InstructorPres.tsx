import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface InstructorPresProps {}

const StyledInstructorPres = styled.div`
  color: pink;
`;

export function InstructorPres(props: InstructorPresProps) {
  return (
    <StyledInstructorPres>
      <h1>Welcome to InstructorPres!</h1>
    </StyledInstructorPres>
  );
}

export default InstructorPres;
