import { render } from '@testing-library/react';

import InstructorPres from './InstructorPres';

describe('InstructorPres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<InstructorPres />);
    expect(baseElement).toBeTruthy();
  });
});
