import { Test } from '@nestjs/testing';
import { InstructorInfraService } from './instructor-infra.service';

describe('InstructorInfraService', () => {
  let service: InstructorInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [InstructorInfraService],
    }).compile();

    service = module.get(InstructorInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
