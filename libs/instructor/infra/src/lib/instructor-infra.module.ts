import { Module } from '@nestjs/common';
import { InstructorInfraService } from './instructor-infra.service';

@Module({
  controllers: [],
  providers: [InstructorInfraService],
  exports: [InstructorInfraService],
})
export class InstructorInfraModule {}
