import { Module } from '@nestjs/common';
import { InstructorAppService } from './instructor-app.service';

@Module({
  controllers: [],
  providers: [InstructorAppService],
  exports: [InstructorAppService],
})
export class InstructorAppModule {}
