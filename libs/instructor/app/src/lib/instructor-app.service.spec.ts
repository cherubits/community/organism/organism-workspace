import { Test } from '@nestjs/testing';
import { InstructorAppService } from './instructor-app.service';

describe('InstructorAppService', () => {
  let service: InstructorAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [InstructorAppService],
    }).compile();

    service = module.get(InstructorAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
