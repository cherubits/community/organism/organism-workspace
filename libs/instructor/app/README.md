# instructor-app

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build instructor-app` to build the library.

## Running unit tests

Run `nx test instructor-app` to execute the unit tests via [Jest](https://jestjs.io).
