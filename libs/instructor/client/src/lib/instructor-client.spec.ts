import { instructorClient } from './instructor-client';

describe('instructorClient', () => {
  it('should work', () => {
    expect(instructorClient()).toEqual('instructor-client');
  });
});
