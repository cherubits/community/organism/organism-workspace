import { Test } from '@nestjs/testing';
import { TreasurerInfraService } from './treasurer-infra.service';

describe('TreasurerInfraService', () => {
  let service: TreasurerInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [TreasurerInfraService],
    }).compile();

    service = module.get(TreasurerInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
