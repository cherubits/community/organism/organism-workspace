import { Module } from '@nestjs/common';
import { TreasurerInfraService } from './treasurer-infra.service';

@Module({
  controllers: [],
  providers: [TreasurerInfraService],
  exports: [TreasurerInfraService],
})
export class TreasurerInfraModule {}
