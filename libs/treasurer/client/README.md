# treasurer-client

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build treasurer-client` to build the library.

## Running unit tests

Run `nx test treasurer-client` to execute the unit tests via [Jest](https://jestjs.io).
