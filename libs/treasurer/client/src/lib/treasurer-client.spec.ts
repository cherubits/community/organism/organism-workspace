import { treasurerClient } from './treasurer-client';

describe('treasurerClient', () => {
  it('should work', () => {
    expect(treasurerClient()).toEqual('treasurer-client');
  });
});
