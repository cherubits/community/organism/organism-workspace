import { Module } from '@nestjs/common';
import { TreasurerAppService } from './treasurer-app.service';

@Module({
  controllers: [],
  providers: [TreasurerAppService],
  exports: [TreasurerAppService],
})
export class TreasurerAppModule {}
