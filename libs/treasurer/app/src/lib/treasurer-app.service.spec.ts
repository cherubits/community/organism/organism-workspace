import { Test } from '@nestjs/testing';
import { TreasurerAppService } from './treasurer-app.service';

describe('TreasurerAppService', () => {
  let service: TreasurerAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [TreasurerAppService],
    }).compile();

    service = module.get(TreasurerAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
