# treasurer-domain

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build treasurer-domain` to build the library.

## Running unit tests

Run `nx test treasurer-domain` to execute the unit tests via [Jest](https://jestjs.io).
