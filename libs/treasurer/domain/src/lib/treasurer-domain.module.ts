import { Module } from '@nestjs/common';
import { TreasurerDomainService } from './treasurer-domain.service';

@Module({
  controllers: [],
  providers: [TreasurerDomainService],
  exports: [TreasurerDomainService],
})
export class TreasurerDomainModule {}
