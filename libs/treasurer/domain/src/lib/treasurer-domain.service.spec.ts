import { Test } from '@nestjs/testing';
import { TreasurerDomainService } from './treasurer-domain.service';

describe('TreasurerDomainService', () => {
  let service: TreasurerDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [TreasurerDomainService],
    }).compile();

    service = module.get(TreasurerDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
