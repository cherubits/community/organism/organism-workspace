# treasurer-common

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build treasurer-common` to build the library.

## Running unit tests

Run `nx test treasurer-common` to execute the unit tests via [Jest](https://jestjs.io).
