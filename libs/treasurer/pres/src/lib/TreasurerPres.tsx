import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface TreasurerPresProps {}

const StyledTreasurerPres = styled.div`
  color: pink;
`;

export function TreasurerPres(props: TreasurerPresProps) {
  return (
    <StyledTreasurerPres>
      <h1>Welcome to TreasurerPres!</h1>
    </StyledTreasurerPres>
  );
}

export default TreasurerPres;
