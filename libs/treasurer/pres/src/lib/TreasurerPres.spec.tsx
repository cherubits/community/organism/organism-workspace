import { render } from '@testing-library/react';

import TreasurerPres from './TreasurerPres';

describe('TreasurerPres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TreasurerPres />);
    expect(baseElement).toBeTruthy();
  });
});
