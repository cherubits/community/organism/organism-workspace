import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface WorksheetPresProps {}

const StyledWorksheetPres = styled.div`
  color: pink;
`;

export function WorksheetPres(props: WorksheetPresProps) {
  return (
    <StyledWorksheetPres>
      <h1>Welcome to WorksheetPres!</h1>
    </StyledWorksheetPres>
  );
}

export default WorksheetPres;
