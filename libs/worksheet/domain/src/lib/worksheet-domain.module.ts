import { Module } from '@nestjs/common';
import { WorksheetDomainService } from './worksheet-domain.service';

@Module({
  controllers: [],
  providers: [WorksheetDomainService],
  exports: [WorksheetDomainService],
})
export class WorksheetDomainModule {}
