import { Test } from '@nestjs/testing';
import { WorksheetDomainService } from './worksheet-domain.service';

describe('WorksheetDomainService', () => {
  let service: WorksheetDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [WorksheetDomainService],
    }).compile();

    service = module.get(WorksheetDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
