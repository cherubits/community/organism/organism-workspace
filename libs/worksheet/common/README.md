# worksheet-common

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build worksheet-common` to build the library.

## Running unit tests

Run `nx test worksheet-common` to execute the unit tests via [Jest](https://jestjs.io).
