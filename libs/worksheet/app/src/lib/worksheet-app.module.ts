import { Module } from '@nestjs/common';
import { WorksheetAppService } from './worksheet-app.service';

@Module({
  controllers: [],
  providers: [WorksheetAppService],
  exports: [WorksheetAppService],
})
export class WorksheetAppModule {}
