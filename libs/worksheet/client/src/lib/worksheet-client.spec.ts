import { worksheetClient } from './worksheet-client';

describe('worksheetClient', () => {
  it('should work', () => {
    expect(worksheetClient()).toEqual('worksheet-client');
  });
});
